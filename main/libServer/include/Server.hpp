#include <string>
#include <fmt/format.h>
#include <iostream>
#include <sstream>
#include <thread>
#include <array>
#include <functional>
#ifdef _WIN32

#define WIN32_LEAN_AND_MEAN
#define NOMINMAX
#include <WinSock2.h>
#include <WS2tcpip.h>

#pragma comment(lib, "Ws2_32.lib")

#else

#include <sys/socket.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <netdb.h>
#include <errno.h>

using SOCKET = int;
#define SD_BOTH SHUT_RDWR
#define closesocket(s) close(s)
#endif


namespace ThunderServer
{
	class ThunderChatServer {
	public :
		//Constructeur de base
		ThunderChatServer(const std::string& client);

		//Constructeur de copie
		ThunderChatServer(const ThunderChatServer& server) = delete; // On ne copie pas un serveur

		//Constructeur de copie par affectation
		ThunderChatServer& operator=(const ThunderChatServer& server) = delete; // On ne copie pas un serveur

		//Constructeur de deplacement
		ThunderChatServer(ThunderChatServer&& server);

		//Constructeur de deplacement par affectation
		ThunderChatServer& operator=(ThunderChatServer&& server);

		//Destructeur
		~ThunderChatServer();


		// ---------- API THUNDER CHAT SERVER METHODS --------------- //

		void OnConnect(std::function<void(const std::string& client)>);

		void OnDisconnect(std::function<void(const std::string& client)>);

		void Stop();

	private :
		bool Start();
		SOCKET serverSocket;
		void Listen();
		SOCKET acceptSocket;
		sockaddr_in _ipServer;
		struct PlayerSocket
		{
			SOCKET socket;
			std::string team; // va devenir int par la suite;
		};
		PlayerSocket _listClients[10];
		std::thread _listThreads[10];
		int _actConnected = 0;
		std::thread _thListen;
	};
}