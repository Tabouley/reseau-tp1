#include <string>
#include <Server.hpp>
#include <fmt/format.h>
#include <iostream>
#include <sstream>
#include <thread>
#include <array>
#include <utility>

#ifdef _WIN32

#define WIN32_LEAN_AND_MEAN
#define NOMINMAX
#include <WinSock2.h>
#include <WS2tcpip.h>

#pragma comment(lib, "Ws2_32.lib")

#else

#include <sys/socket.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <netdb.h>
#include <errno.h>

using SOCKET = int;
#define SD_BOTH SHUT_RDWR
#define closesocket(s) close(s)
#endif
#include <functional>






namespace ThunderServer
{
	ThunderChatServer::ThunderChatServer(const std::string& client)
	{
		//Parsing IP 
		if (client.find("[")>=0)
		{
			//IPV6 with PORT
			
		}
		else
		{

		}

		//ON MET L'IP DANS LA VARIABLE
		_ipServer.sin_family = AF_INET;// AF_INET POUR IPV4 SINON AF_INET6 POUR IPV6
		_ipServer.sin_port = htons(8888);// LE PORT


		if (inet_pton(AF_INET,"127.0.0.1", &(_ipServer.sin_addr)) < 0)
		{
			std::cout << "L'ip fournie n'est pas valide";
			//throw std::exception("L'ip fournie n'est pas valide");
			return;
		}



		// STARTING SERVER
		Start();


	};
		
	ThunderChatServer::ThunderChatServer(ThunderChatServer&& server) {

	};

	ThunderChatServer& ThunderChatServer::operator=(ThunderChatServer&& server)
	{
		// TODO: ins�rer une instruction return ici
		return server;
	};

	ThunderChatServer::~ThunderChatServer()
	{

	};


	void ThunderChatServer::OnConnect(std::function <void(const std::string& client)>) {
		
	};

	void ThunderChatServer::OnDisconnect(std::function<void(const std::string& client)>) {

	};

	void ThunderChatServer::Stop() {

	};
	
	void ListenClient(SOCKET actClient , int idClient)
	{
		std::array<char, 1024> buffer;
		int receivedBytes = recv(actClient, buffer.data(), 1024, 0);
		if (receivedBytes < 0)
		{
			std::cout << "Error" << std::endl;
			closesocket(actClient);
			return ;
		}

		std::string dataReceived = std::string(buffer.data(), receivedBytes).c_str();
		std::cout << dataReceived << std::endl;
		




	};
	 void ThunderChatServer::Listen()
	{
		while (1)
		{
			// SI ON EST DEJA 10 ON STOPPE LE LISTENING DE CONNEXIONS
			if (_actConnected >= 10)
			{
				break;
			}
			// ON ACCEPTE UN NOUVEAU CLIENT ( SI PERSONNE VEUT SE CONNECTER CA SE BLOQUE )
			sockaddr clientAddr;
			socklen_t clientAddrSize = sizeof(sockaddr);
			SOCKET client = accept(serverSocket, &clientAddr, &clientAddrSize);
			if (client < 0)
			{
				std::cout << "Error";
				closesocket(client);
				return;
			}
			
			// ON SE PREPARE A RECEVOIR LE NUMERO DE L'EQUIPE QUE LE CLIENT NOUS ENVOIE JUSTE APRES SA CONNEXION
			std::array<char, 1024> buffer;
			int receivedBytes = recv(client, buffer.data(), 1024, 0);
			if (receivedBytes < 0)
			{
				std::cout << "Error" << std::endl;
				closesocket(client);
				return;
			}
			
			// ON ENREGISTRE LE SOCKET DU CLIENT DANS NOTRE LISTE
			_listClients[_actConnected].socket = client;
			// ON ENREGISTRE SON NUMERO D'EQUIPE			
			_listClients[_actConnected].team = std::string(buffer.data(), receivedBytes).c_str();
			std::cout << _listClients[_actConnected].team;
			//ON FAIT UN CALLBACK ( je sais pas ce que c'est )
			//OnConnect(); // call callback
			/*
			// ON LANCE LE THREAD QUI VA ECOUTER TOUT CE QUE LE CLIENT A A NOUS DIRE
			act._listThreads[act._actConnected] = std::thread(&ListenClient,client,act._actConnected);
			// ON INCREMENTE LE NOMBRE DE PERSONNES CONNECTES
			act._actConnected++;
			*/
		}

	}


	bool ThunderChatServer::Start()
	{
		//INIT W2SOCK
		#ifdef _WIN32
		WSAData wsaData;
		if (WSAStartup(MAKEWORD(2, 2), &wsaData) != 0)
		{
			std::cout << " Erreur lors de l'initialisation du serveur WinSock : \n" << WSAGetLastError();
			return false;
		}
		#endif
		//INIT SOCKET
		serverSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
		if (serverSocket<0)
		{
			std::cout << "Erreur lors de l'initialisation du socket : " << std::endl;
			return false;
		}

		if (bind(serverSocket, reinterpret_cast<sockaddr*>(&_ipServer), sizeof(sockaddr)) < 0)
		{
			std::cout << "Erreur lors du binding de la socket � l'adresse ip fournie" ;
			closesocket(serverSocket);
			return false;
		}
		
		if (listen(serverSocket, 10) < 0)
		{
			std::cout << "Erreur lors de l'�coute sur la socket";
			closesocket(serverSocket);
			return false;
		}
		Listen();
		
	};


}