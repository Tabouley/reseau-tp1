namespace ThunderServer
{
	class Player
	{
	private :
		std::string _pseudo;
		int _team;

	public:
		Player(const std::string& psd, const int& tm) :_team(tm), _pseudo(psd)
		{

		}
		std::string getPseudo()
		{
			return _pseudo;
		}

	};
}