#ifdef _WIN32

#define WIN32_LEAN_AND_MEAN
#define NOMINMAX
#include <WinSock2.h>
#include <WS2tcpip.h>

#pragma comment(lib, "Ws2_32.lib")

#else

#include <sys/socket.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <netdb.h>
#include <errno.h>

using SOCKET = int;
#define SD_BOTH SHUT_RDWR
#define closesocket(s) close(s)
#endif

#include <array>
#include <iostream>


int constructor()
{
	//INIT W2SOCK
	#ifdef _WIN32

	WORD versionRequested;
	WSADATA wsaData;
	versionRequested = MAKEWORD(2, 2);
	if (WSAStartup(versionRequested, &wsaData) < 0)
	{
		std::cout << "Error";
		return EXIT_FAILURE;
	}

	#endif
	//INIT SOCKET
	std::cout << "Lancement du serveur..." << std::endl;

	SOCKET serverSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

    if (serverSocket < 0)
	{
		std::cout << "Erreur lors de l'initialisation du socket : " << std::endl;
		return EXIT_FAILURE;
	}

	sockaddr_in address;
	address.sin_family = AF_INET;
	address.sin_port = htons(8888);
	if (inet_pton(AF_INET, "127.0.0.1", &(address.sin_addr)) < 0)
	{
		std::cout << "Error";
		return EXIT_FAILURE;
	}


	if (bind(serverSocket, reinterpret_cast<sockaddr*>(&address), sizeof(sockaddr)) < 0)
	{
		std::cout << "Erreur lors du binding de la socket � l'adresse ip fournie";
		closesocket(serverSocket);
		return EXIT_FAILURE;
	}
	std::cout << "En attente de connexions ..." << std::endl;
	if (listen(serverSocket, 10) < 0)
	{
		std::cout << "Erreur lors de l'�coute sur la socket";
		closesocket(serverSocket);
		return EXIT_FAILURE;
	}

	sockaddr clientAddr;
	socklen_t clientSize = sizeof(sockaddr);
	SOCKET client = accept(serverSocket, &clientAddr, &clientSize);

	if (client < 0)
	{
		std::cout << "Erreur lors de la connexion du client";
		closesocket(serverSocket);
		return EXIT_FAILURE;

	}
    std::cin.ignore();
    std::array<char, 512> ipClientStr;
	std::cout << "Client : " << inet_ntop(clientAddr.sa_family, &client, ipClientStr.data(), 512) << " is mb connected" << std::endl;

	std::array<char, 1024> buffer;
	int receivedBytes = recv(serverSocket, buffer.data(), 1024, 0);
	if (receivedBytes < 0)
	{
		std::cout << " Error lors de la reception du message" << std::endl;
		closesocket(serverSocket);
		return EXIT_FAILURE;

	}
	std::cout << "I received : " << std::string(buffer.data(), receivedBytes).c_str() << std::endl;
	/*if (receivedBytes != sentBytes)
	{
		std::cout << "Error" << std::endl;
		closesocket(serverSocket);
		return EXIT_FAILURE;
	}*/
	shutdown(client, SD_BOTH);
	closesocket(client);

	shutdown(serverSocket, SD_BOTH);
	closesocket(serverSocket);

	#ifdef _WIN32
		WSACleanup();
	#endif


}

int OnConnect()
{
	return 0;

}

int OnDisconnect()
{
	return 0;
}


