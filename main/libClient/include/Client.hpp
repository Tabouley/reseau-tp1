#include <string>
#include <functional>
#ifdef _WIN32

#define WIN32_LEAN_AND_MEAN
#define NOMINMAX
#include <WinSock2.h>
#include <WS2tcpip.h>

#pragma comment(lib, "Ws2_32.lib")

#else

#include <sys/socket.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <netdb.h>
#include <errno.h>

using SOCKET = int;
#define SD_BOTH SHUT_RDWR
#define closesocket(s) close(s)
#endif

namespace ThunderClient
{
	class ThunderChatClient {
	public:
		ThunderChatClient(std::string serverAddress, std::string playerName, std::string team);

		bool Connect(std::string serverAddress);

		void OnMessage();

		void OnDisconnect();

		bool SendToParty(const std::string& msg);

		bool SendToTeam(const std::string& msg);

		~ThunderChatClient();

	private:
		sockaddr_in _ipServer;
		SOCKET sosset;
	};

}