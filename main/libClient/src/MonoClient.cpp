
#ifdef _WIN32
#define WIN32_LEAN_AND_MEAN
#define NOMINMAX
#include <WinSock2.h>
#include <WS2tcpip.h>
#pragma comment(lib, "Ws2_32.lib")
#else
#include <sys/socket.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <netdb.h>
#include <errno.h>
using SOCKET = int;
#define SD_BOTH SHUT_RDWR
#define closesocket(s) close(s)
#endif
#include <string>
#include <functional>
#include <../include/MonoClient.hpp>
#include <iostream>



	MonoClient::MonoClient(const std::string& serverAddress,const std::string& playerName, const std::string& team): _serverAddr(serverAddress),_playerName(playerName),_team(team)
	{
		_address.sin_family = AF_INET;
		_address.sin_port = htons(8888); // 8888 � remplacer par le port recuperer dans l'argument
		if (inet_pton(AF_INET, "127.0.0.1", &(_address.sin_addr)) < 0) // 127.0.0.1 � rempalcer par le port recuper� dans l'argument
		{
			std::cout << "Error";
			return;
		}
	}

	int MonoClient::Connect()
	{
		//INIT W2SOCK
		#ifdef _WIN32
		WSAData wsaData;
		if (WSAStartup(MAKEWORD(2, 2), &wsaData) != 0)
		{
			std::cout << " Erreur lors de l'initialisation du serveur WinSock : \n" << WSAGetLastError();
			return EXIT_FAILURE;
		}
		#endif
		//INIT SOCKET
		SOCKET sosset = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
		if (sosset < 0)
		{
			std::cout << "Erreur lors de l'initialisation du socket : " << std::endl;
			return false;
		}
		if (connect(sosset, reinterpret_cast<sockaddr*>(&_address), sizeof(sockaddr)) < 0)
		{
			std::cout << "Erreur lors de la connexion au serveur : " << std::endl;
			return false;
		}
        std::cin.ignore();
        std::string msg = "Hellow World";

		int sendResult = send(sosset,msg.c_str(), msg.size() + 1, 0);

	}


	void OnDisconnect()
	{

	}

	void SendToParty(const std::string& msg)
	{

	}

	void SendToTeam(const std::string& msg)
	{

	}

