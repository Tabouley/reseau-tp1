#include <string>
#include <Client.hpp>
#include <iostream>
#include <sstream>
#include <thread>
#include <array>
#ifdef _WIN32

#define WIN32_LEAN_AND_MEAN
#define NOMINMAX
#include <WinSock2.h>
#include <WS2tcpip.h>

#pragma comment(lib, "Ws2_32.lib")

#else

#include <sys/socket.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <netdb.h>
#include <errno.h>

using SOCKET = int;
#define SD_BOTH SHUT_RDWR
#define closesocket(s) close(s)
#endif
#include <functional>



namespace ThunderClient
{
	ThunderChatClient::ThunderChatClient(std::string serverAddress, std::string playerName, std::string team)
	{
		_ipServer.sin_family = AF_INET;// AF_INET POUR IPV4 SINON AF_INET6 POUR IPV6
		_ipServer.sin_port = htons(8888);// LE PORT
		PCSTR ipString = "0.0.0.0"; // L'IP
		if (inet_pton(AF_INET, ipString, &(_ipServer.sin_addr)) < 0)
		{
			std::cout << "L'ip fournie n'est pas valide";
			throw std::exception("L'ip fournie n'est pas valide");
		}




	}

	bool ThunderChatClient::Connect(std::string serverAddress) {
		//Connect client to the serverAddress
		//No exception here. Set a bool to false if there's an error.

		//INIT W2SOCK
	#ifdef _WIN32
		WSAData wsaData;
		if (WSAStartup(MAKEWORD(2, 2), &wsaData) != 0)
		{
			std::cout << " Erreur lors de l'initialisation du serveur WinSock : \n" << WSAGetLastError();
			return false;
		}
	#endif
		//INIT SOCKET
		sosset = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
		if (sosset < 0)
		{
			std::cout << "Erreur lors de l'initialisation du socket : " << WSAGetLastError();
			return false;
		}
		if (connect(sosset, reinterpret_cast<sockaddr*>(&_ipServer), sizeof(sockaddr)) < 0)
		{
			std::cout << "Erreur lors de la connexion au serveur : " << WSAGetLastError();
			return false;
		}



	}

	void ThunderChatClient::OnMessage() { //Type Message ?
		//Event callback on message reception
		//Callback type : std::function<void(const Message& msg)>
	}

	void ThunderChatClient::OnDisconnect() {
		//Event callback on client disconnection
		//Callback type : std::function<void()>
	}

	bool ThunderChatClient::SendToParty(const std::string& msg) {
		//Send the msg to the whole party -> broadcast ?
	}

	bool ThunderChatClient::SendToTeam(const std::string& msg) {
		//Send the message to the team only
	}

	ThunderChatClient::~ThunderChatClient() {
		//Stop and disconnect the client from the server
	}



}